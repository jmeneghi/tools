#!/bin/bash

print_usage() {
	echo "Usage: to-upstream-hashes [--oneline] [rhel_hashes_file]"
	echo "If no argument is provided, stdin will be read"
	echo "The input must be a list of git commits (git log --oneline or git rev-list)"
	echo "This must be executed from an updated RHEL kernel tree"
}

exit_error() {
	echo "Error: $1" >&2
	exit 1
}

# parse args
while (( $# > 0 )); do
	case "$1" in
		-h|--help)
			print_usage
			exit;;
		--oneline)
			ONELINE_FMT=true
			;;
		*)
			[[ -n $input_file ]] && exit_error "Please provide only one input file"
			input_file="$1"
			;;
	esac
	shift
done

# default values
ONELINE_FMT=${ONELINE_FMT:-false}
input_file=${input_file:-/dev/stdin}

if [[ $ONELINE_FMT == true ]]; then
	short_hash=$(git log -1 --pretty=%h)
	SHORT_HASH_LEN=${#short_hash}
fi

while IFS= read -r line; do
	rhel_hash="$(awk '{print $1}' <<< "$line")"
	upstream_hash_data="$(git log -1 --pretty=%B "$rhel_hash")"

	upstream_status="$(echo "$upstream_hash_data" | awk '/^Upstream Status:/ {print $3}' | head -1)"
	if [[ $upstream_status == RHEL* ]]; then
		# skip if RHEL only commit
		echo "WARN: can't parse upstream hash for $rhel_hash (RHEL Only)" >&2
		continue
	fi

	# general "Commit: abcdef123456..." case
	upstream_hash="$(echo "$upstream_hash_data" | awk '/^commit ([0-9a-f]{40})/ {print $2}')"

	if [[ $upstream_hash == "" ]]; then
		# check to see if the rhel commit was created via git-cherry-pick
		upstream_hash="$(echo "$upstream_hash_data" | awk '/^\(cherry picked from commit/ {print $5}' | tr -d ')')"
	fi

	if [[ $upstream_hash == "" ]]; then
		if [[ $upstream_status == "" ]]; then
			echo "WARN: can't parse upstream hash for $rhel_hash (unknown reason)" >&2
		else
			echo "WARN: can't parse upstream hash for $rhel_hash ($upstream_status)" >&2
		fi
		continue
	fi

	# OUTPUT
	if [[ $ONELINE_FMT == false ]]; then
		echo "$upstream_hash"
	else
		commit_descr="$(git log -1 --pretty=%s "$rhel_hash")"
		upstream_short_hash=${upstream_hash::$SHORT_HASH_LEN}
		echo "$upstream_short_hash $commit_descr"
	fi
done < "$input_file"
